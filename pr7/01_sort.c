#include <stdio.h>
#include <stdlib.h>

void QuickSort(int *arr, int a, int b, int n, FILE *log);

void PiramidSort(int *arr, int arr_len, FILE *log);

void AddPyramid(int arr[], int root, int bot);

void Swap(int *a, int *b);



int main() {
    int n, i = 0, *flowinput, *flowinput2;
    FILE *log;
    scanf("%d", &n);
    flowinput = malloc(sizeof(int)*n);
    flowinput2 = malloc(sizeof(int)*n);

    while(scanf("%d", &flowinput[i++]) == 1) {
        flowinput2[i-1] = flowinput[i-1];
        if(i == n) {
            break;
        }
    }

    log = fopen("quicksort.log", "w");
    QuickSort(flowinput, 0, n-1, n, log);
    fclose(log);

    log = fopen("heapsort.log", "w");
    PiramidSort(flowinput2, n, log);
    fclose(log);

    printf("\n");
    for (i=0; i<n; i++) {
        printf("%d ", flowinput2[i]);
    }
    return 0;
}

void QuickSort(int *arr, int a, int b, int n, FILE *log) {
    int i;
    if (a < b)
    {
        int begin = a, end = b, middle = arr[(begin + end) / 2];
        do {
            while (arr[begin] < middle) begin++;
            while (arr[end] > middle) end--;
            if (begin <= end) {
                fprintf(log, "\n");
                Swap(&arr[begin],&arr[end]);
                begin++;
                end--;
                for (i=0; i<n; i++) {
                    fprintf(log, "%d ", arr[i]);
                }
            }
        } while (begin <= end);
        QuickSort(arr, a, end, n, log);
        QuickSort(arr, begin, b, n, log);
    }
}

void PiramidSort(int *arr, int arr_len, FILE *log) {
    for (int i = (arr_len / 2) - 1; i >= 0; i--)
    {
        for (int j=0; j<arr_len; j++) {
            fprintf(log, "%d ", arr[j]);
        }
        fprintf(log, "\n");
        AddPyramid(arr, i, arr_len - 1);
    }
    for (int i = arr_len - 1; i >= 1; i--) {
        Swap(&arr[0], &arr[i]);
        AddPyramid(arr, 0, i - 1);
        for (int j=0; j<arr_len; j++) {
            fprintf(log, "%d ", arr[j]);
        }
        fprintf(log, "\n");
    }
}

void AddPyramid(int arr[], int root, int bot) {
    int max;
    int flag = 0;
    while ((root * 2 <= bot) && (!flag))
    {
        if (root * 2 == bot) {
            max = root * 2;
        } else if (arr[root * 2] > arr[root * 2 + 1]) {
            max = root * 2;
        } else {
            max = root * 2 + 1;
        }
        if (arr[root] < arr[max]) {
            Swap(&arr[root], &arr[max]);
            root = max;
        } else {
            flag = 1;
        }
    }
}

void Swap(int *x, int *y) {
    if (x != y) {
        *x ^= *y;
        *y ^= *x;
        *x ^= *y;
    }
}
