#include <stdio.h>
#include <stdlib.h>

//Очередь
typedef struct queue {
    struct queue_node *head;
    struct queue_node *tail;
} queue;

typedef struct queue_node {
    void *object;
    struct queue_node *next;
} queue_node;


//Инициализация очереди
void queue_init(queue *qu);

//Добаление объекта в очередь
void queue_push(queue *qu, void *obj);

//Вывод указателя на объект из очереди
void *queue_next(queue *qu);

//Удаление элемента очереди по указателю
void queue_delete_node(queue_node *nd);

//Очистка очереди
void queue_clear(queue *qu);


//Дерево
typedef struct tree {
    struct node *head;

} tree;

typedef struct node {
    int value;
    struct node *parent;
    struct node *left;
    struct node *right;
} node;

// Инициализация дерева
void init(tree* t);

// Вставка значения в дерево:
int insert(tree* t, int value);

int main() {
    int n, i, *flowinput;
    tree mytree;
    init(&mytree);
    queue q;
    queue_init(&q);

    n = 7;
    flowinput = malloc(sizeof(int)*n);
    i = 0;
    while(scanf("%d", &flowinput[i++]) == 1) {
        (void) insert(&mytree, flowinput[i-1]);
        if(i == n) {
            free(flowinput);
            break;
        }
    }

    node *nd;
    queue_push(&q, mytree.head);
    while(q.head != NULL) {
        nd = queue_next(&q);
        printf("%d ", nd->value);
        if(nd->left != NULL) {
            queue_push(&q, nd->left);
        }
        if(nd->right != NULL) {
            queue_push(&q, nd->right);
        }
    }
    return 0;
}

void queue_init(queue *qu) {
    qu->head = NULL;
    qu->tail = NULL;
}

void queue_push(queue *qu, void *obj) {
    queue_node *node_push = malloc(sizeof(node_push));
    node_push->object = obj;
    node_push->next = NULL;
    if(qu->head == NULL) {
        qu->head = node_push;
        qu->tail = node_push;
    } else {
        qu->tail->next = node_push;
        qu->tail = node_push;
    }
}

void *queue_next(queue *qu) {
    if(qu->head == NULL) {
        return NULL;
    } else {
        void *obj_return = qu->head->object;
        queue_node *node_to_delete = qu->head;
        qu->head = qu->head->next;
        queue_delete_node(node_to_delete);
        return  obj_return;
    }
}

void queue_delete_node(queue_node *nd) {
    nd->next = NULL;
    nd->object = NULL;
    free(nd);
}

void queue_clear(queue *qu) {
    queue_node *next_node = qu->head;
    queue_node *node_to_delete;

    while(next_node != NULL) {
        node_to_delete = next_node;
        next_node = node_to_delete->next;
        queue_delete_node(node_to_delete);
    }

    queue_init(qu);
}

void init(tree* t) {
    t->head = NULL;
}

int insert(tree* t, int value) {
    node *insert;
    insert = malloc(sizeof(node));
    insert->value = value;
    insert->parent = NULL;
    insert->right = NULL;
    insert->left = NULL;

    if(t->head == NULL) {
        t->head = insert;
        return 0;
    } else {
        node *currentnode = t->head;
        while(1) {
            if(currentnode->value == value) {
                return 1;
            } else if(currentnode->value > value) {
                if(currentnode->left == NULL) {
                    insert->parent = currentnode;
                    currentnode->left = insert;
                    break;
                }
                currentnode = currentnode->left;
            } else {
                if(currentnode->right == NULL) {
                    insert->parent = currentnode;
                    currentnode->right = insert;
                    break;
                }
                currentnode = currentnode->right;
            }
        }
        return 0;
    }
}
