#include <stdio.h>
#include <stdlib.h>

//Дерево
typedef struct tree {
    struct node *head;

} tree;

typedef struct node {
    int value;
    struct node *parent;
    struct node *left;
    struct node *right;
} node;

// Инициализация дерева
void init(tree* t);

// Вставка значения в дерево:
int insert(tree* t, int value);

void post_order(node * n) {
    if (n != NULL) {
        post_order(n->left);
        post_order(n->right);
        printf("%d ",n->value);
    }
}


int main() {
    int n, i, *flowinput;
    tree mytree;
    init(&mytree);

    n = 7;
    flowinput = malloc(sizeof(int)*n);
    i = 0;
    while(scanf("%d", &flowinput[i++]) == 1) {
        (void) insert(&mytree, flowinput[i-1]);
        if(i == n) {
            free(flowinput);
            break;
        }
    }

    post_order(mytree.head);
    return 0;
}

void init(tree* t) {
    t->head = NULL;
}

int insert(tree* t, int value) {
    node *insert;
    insert = malloc(sizeof(node));
    insert->value = value;
    insert->parent = NULL;
    insert->right = NULL;
    insert->left = NULL;

    if(t->head == NULL) {
        t->head = insert;
        return 0;
    } else {
        node *currentnode = t->head;
        while(1) {
            if(currentnode->value == value) {
                return 1;
            } else if(currentnode->value > value) {
                if(currentnode->left == NULL) {
                    insert->parent = currentnode;
                    currentnode->left = insert;
                    break;
                }
                currentnode = currentnode->left;
            } else {
                if(currentnode->right == NULL) {
                    insert->parent = currentnode;
                    currentnode->right = insert;
                    break;
                }
                currentnode = currentnode->right;
            }
        }
        return 0;
    }
}
