#include <stdio.h>

int main() {
  int n, i;
  double sum = 0, input;
  (void) scanf("%i", &n);
  for(i = 0; i < n; i++) {
    (void) scanf("%lf", &input);
    sum += input;
  }
  printf("%lf", sum/n);
  return 0;
}