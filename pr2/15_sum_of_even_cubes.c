#include <stdio.h>
#include <math.h>

int main() {
  int n, i;
  double sum = 0, input;
  (void) scanf("%i", &n);
  for(i = 0; i < n; i++) {
    (void) scanf("%lf", &input);
    sum += (i % 2) * pow(input, 3);
  }
  printf("%lf", sum);
  return 0;
}