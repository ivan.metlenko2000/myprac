#include <stdio.h>
#include <stdlib.h>

//Очередь
typedef struct queue {
    struct queue_node *head;
    struct queue_node *tail;
} queue;

typedef struct queue_node {
    void *object;
    struct queue_node *next;
} queue_node;


//Инициализация очереди
void queue_init(queue *qu);

//Добаление объекта в очередь
void queue_push(queue *qu, void *obj);

//Вывод указателя на объект из очереди
void *queue_next(queue *qu);

//Удаление элемента очереди по указателю
void queue_delete_node(queue_node *nd);

//Очистка очереди
void queue_clear(queue *qu);


//Дерево
typedef struct tree {
    struct node *head;

} tree;

typedef struct node {
    int value;
    struct node *parent;
    struct node *left;
    struct node *right;
} node;

// Инициализация дерева
void init(tree* t);

// Удалить все элементы из дерева
void clean(tree* t);

// Поиск элемента по значению. Вернуть NULL если элемент не найден
node* find(tree* t, int value);

// Вставка значения в дерево:
// 0 - вставка выполнена успешно
// 1 - элемент существует
// 2 - не удалось выделить память для нового элемента
int insert(tree* t, int value);

// Удалить элемент из дерева:
// 0 - удаление прошло успешно
// 1 - нет элемента с указанным значением
int remove_node(tree* t, int value);

// Удалить минимальный элемент из поддерева, корнем которого является n
// Вернуть значение удаленного элемента
int remove_min(node* n);

// Выполнить правое вращение поддерева, корнем которого является n:
// 0 - успешно выполненная операция
// 1 - вращение невозможно
int rotate_right(node* n);

// Выполнить левое вращение поддерева, корнем которого является n:
// 0 - успешно выполненная операция
// 1 - вращение невозможно
int rotate_left(node* n);

// Вывести все значения из поддерева, корнем которого является n
// по уровням начиная с корня.
// Каждый уровень выводится на своей строке.
// Элементы в строке разделяются пробелом. Если элемента нет, заменить на _.
// Если дерево пусто, вывести -
void print(node* n);

// Вывести все значения дерева t, аналогично функции print
void print_tree(tree* t);

//Считывает число и ищет элемент с заданным значение выводит значение предка и потомков
void print_node(tree *t, int input);

//Считает количество элементов в поддереве
int count_nodes(node *n);

void clean_node(node* n);


int main() {
    tree mytree;
    init(&mytree);

    int n, i, *flowinput, input;
    n = 4;
    flowinput = malloc(sizeof(int)*n);
    i = 0;
    while(scanf("%d", &flowinput[i++]) == 1) {
        (void) insert(&mytree, flowinput[i-1]);
        if(i == n) {
            free(flowinput);
            break;
        }
    }
    print_tree(&mytree);

    n = 3;
    flowinput = malloc(sizeof(int)*n);
    i = 0;
    while(scanf("%d", &flowinput[i++]) == 1) {
        (void) insert(&mytree, flowinput[i-1]);
        if(i == n) {
            free(flowinput);
            break;
        }
    }
    print_tree(&mytree);

    scanf("%d", &input);
    print_node(&mytree, input);

    scanf("%d", &input);
    print_node(&mytree, input);

    scanf("%d", &input);
    remove_node(&mytree, input);
    print_tree(&mytree);

    while(rotate_left(mytree.head) == 0) {
        mytree.head = mytree.head->parent;
    }
    print_tree(&mytree);

    while(rotate_right(mytree.head) == 0) {
        mytree.head = mytree.head->parent;
    }
    print_tree(&mytree);

    printf("%d\n\n", count_nodes(mytree.head));

    clean(&mytree);
    print_tree(&mytree);

    return 0;
}

void queue_init(queue *qu) {
    qu->head = NULL;
    qu->tail = NULL;
}

void queue_push(queue *qu, void *obj) {
    queue_node *node_push = malloc(sizeof(node_push));
    node_push->object = obj;
    node_push->next = NULL;
    if(qu->head == NULL) {
        qu->head = node_push;
        qu->tail = node_push;
    } else {
        qu->tail->next = node_push;
        qu->tail = node_push;
    }
}

void *queue_next(queue *qu) {
    if(qu->head == NULL) {
        return NULL;
    } else {
        void *obj_return = qu->head->object;
        queue_node *node_to_delete = qu->head;
        qu->head = qu->head->next;
        queue_delete_node(node_to_delete);
        return  obj_return;
    }
}

void queue_delete_node(queue_node *nd) {
    nd->next = NULL;
    nd->object = NULL;
    free(nd);
}

void queue_clear(queue *qu) {
    queue_node *next_node = qu->head;
    queue_node *node_to_delete;

    while(next_node != NULL) {
        node_to_delete = next_node;
        next_node = node_to_delete->next;
        queue_delete_node(node_to_delete);
    }

    queue_init(qu);
}

void init(tree* t) {
    t->head = NULL;
}

void clean(tree* t) {
    clean_node(t->head);
    t->head = NULL;
}

node* find(tree* t, int value) {
    node *current_node = t->head;
    while (current_node != NULL) {
        if(current_node->value == value) {
            return current_node;
        } else if(current_node->value > value) {
            current_node = current_node->left;
        } else {
            current_node = current_node->right;
        }
    }
    return NULL;
}

int insert(tree* t, int value) {
    node *insert;
    insert = malloc(sizeof(node));
    insert->value = value;
    insert->parent = NULL;
    insert->right = NULL;
    insert->left = NULL;

    if(t->head == NULL) {
        t->head = insert;
        return 0;
    } else {
        node *currentnode = t->head;
        while(1) {
            if(currentnode->value == value) {
                return 1;
            } else if(currentnode->value > value) {
                if(currentnode->left == NULL) {
                    insert->parent = currentnode;
                    currentnode->left = insert;
                    break;
                }
                currentnode = currentnode->left;
            } else {
                if(currentnode->right == NULL) {
                    insert->parent = currentnode;
                    currentnode->right = insert;
                    break;
                }
                currentnode = currentnode->right;
            }
        }
        return 0;
    }
}

int remove_node(tree* t, int value) {
    node *node_to_delete;
    node_to_delete = find(t, value);
    if(node_to_delete != NULL) {
        if(node_to_delete->parent == NULL) {
            if(node_to_delete->left == NULL && node_to_delete->right == NULL) {
                clean(t);
            } else if(node_to_delete->left == NULL || node_to_delete->right == NULL) {
                if(node_to_delete->left == NULL) {
                    t->head = node_to_delete->right;
                    free(node_to_delete);
                } else {
                    t->head = node_to_delete->left;
                    free(node_to_delete);
                }
            } else {
                node *child_node = node_to_delete->right;
                while (child_node->left != NULL) {
                    child_node = child_node->left;
                }
                int new_value = child_node->value;
                remove_node(t, child_node->value);
                node_to_delete->value = new_value;
            }
            return 0;
        }
        if(node_to_delete->left == NULL && node_to_delete->right == NULL) {
            if(node_to_delete->parent->value > node_to_delete->value) {
                node_to_delete->parent->left = NULL;
            } else {
                node_to_delete->parent->right = NULL;
            }
            free(node_to_delete);
        } else if(node_to_delete->left == NULL || node_to_delete->right == NULL) {
            node *child_node;
            if(node_to_delete->left != NULL) {
                child_node = node_to_delete->left;
            } else {
                child_node = node_to_delete->right;
            }
            child_node->parent = node_to_delete->parent;
            if(node_to_delete->parent->value > node_to_delete->value) {
                node_to_delete->parent->left = child_node;
            } else {
                node_to_delete->parent->right = child_node;
            }
        } else {
            node *child_node = node_to_delete->right;
            while (child_node->left != NULL) {
                child_node = child_node->left;
            }
            int new_value = child_node->value;
            remove_node(t, child_node->value);
            node_to_delete->value = new_value;
        }
        return 0;
    }
    return 1;
}

int remove_min(node* n) {
    node *node_to_delete = n;
    if(node_to_delete == NULL) {
        return NULL;
    }
    while(node_to_delete->left != NULL) {
        node_to_delete = node_to_delete->left;
    }
    int return_value = node_to_delete->value;
    tree t;
    t.head = n;
    remove_node(&t, node_to_delete);
    return return_value;
}

int rotate_right(node* n) {
    if(n == NULL || n->left == NULL) {
        return 1;
    } else {
        node *pivot = n->left, *root = n;
        root->left = pivot->right;
        pivot->right = root;
        pivot->parent = root->parent;
        root->parent = pivot;
        return 0;
    }
}

int rotate_left(node* n) {
    if(n == NULL || n->right == NULL) {
        return 1;
    } else {
        node *pivot = n->right, *root = n;
        root->right = pivot->left;
        pivot->left = root;
        pivot->parent = root->parent;
        root->parent = pivot;
        return 0;
    }
}

void print(node* n) {
    int width = 0, target_width = 1, itsTimeToStop = 0;
    void *out;
    //очереди для нод дерева и текста закарючек
    queue tree_nodes, promise_text;
    queue_init(&tree_nodes);
    queue_init(&promise_text);
    node *node_display = n;
    if(n == NULL) {
        printf("%s", "-\n\n");
        return;
    }
    while(1) {
        if(node_display == NULL) {
            if(target_width == 1) {
                printf("%s", "_\n");
            }
            queue_push(&promise_text, (char)95);
            queue_push(&tree_nodes, NULL);
            queue_push(&tree_nodes, NULL);
            node_display = queue_next(&tree_nodes);
        } else {
            itsTimeToStop = 1;
            out = queue_next(&promise_text);
            while(out != NULL) {
                printf("%c", (char)out);
                out = queue_next(&promise_text);
            }
            printf("%d", node_display->value);
            queue_push(&tree_nodes, node_display->left);
            queue_push(&tree_nodes, node_display->right);
            node_display = queue_next(&tree_nodes);
        }

        width++;
        if(width == target_width) {
            if(itsTimeToStop == 0) {
                printf("%s", "\n");
                break;
            }
            target_width *= 2;
            width = 0;
            itsTimeToStop = 0;
            out = queue_next(&promise_text);
            while(out != NULL) {
                printf("%c", (char)out);
                out = queue_next(&promise_text);
            }
            printf("%s", "\n");
        } else {
            queue_push(&promise_text, (char)32);
        }
    }
    queue_clear(&tree_nodes);
    queue_clear(&promise_text);
}

void print_tree(tree* t) {
    print(t->head);
}

void print_node(tree *t, int input) {
    node *find_result = find(t, input);
    if(find_result != NULL) {
        if(find_result->parent != NULL) {
            printf("%d", find_result->parent->value);
        } else {
            printf("%s", "_");
        }

        if(find_result->left != NULL) {
            printf(" %d", find_result->left->value);
        } else {
            printf(" %s", "_");
        }

        if(find_result->right != NULL) {
            printf(" %d", find_result->right->value);
        } else {
            printf(" %s", "_");
        }
    } else {
        printf("%s", "-");
    }
    printf("%s", "\n\n");
}

void clean_node(node* n) {
    if (n != NULL) {
        clean_node(n->left);
        clean_node(n->right);
        free(n);
    }
}

int count_nodes(node *n) {
    if (n == NULL) {
        return 0;
    }
    return count_nodes(n->right) + count_nodes(n->left) + 1;
}
