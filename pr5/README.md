# Практическая работа №5

## Задание

Написать на языке программирования C динамическую структуру данных “бинарное дерево поиска” для хранения целочисленных значений.

## Ход работы

### Что такое бинарное дерево

Двоичное дерево поиска — это двоичное дерево, для которого выполняются следующие дополнительные условия:

- Оба поддерева — левое и правое — являются двоичными деревьями поиска.
- У всех узлов левого поддерева произвольного узла X значения ключей данных меньше, нежели значение ключа данных самого узла X.
- У всех узлов правого поддерева произвольного узла X значения ключей данных больше либо равны, нежели значение ключа данных самого узла X.

То есть структура узла дере будет указателя на правого потомка, левого, а также ключ данного узла, по необходимости можно ввести указатель на предка этого узла.

Само дерево будет состоять из указателя на корень дерева, а также если необходимо счётчик элементов дерева.

Основные операции с деревом проще выполнять рекурсивно, но некоторые задачи не выполняются рекурсивно, например поиск по дереву в ширину.

Поэтому функции основные функции, которые на вход получают дерево зачастую совершают некоторые проверки, а затем вызывают рекурсивную функцию, в которую подаётся узел дерева.

Основной операцией является ввод данных, который осуществляется таким образом, что по дереву проще проводить поиск, то есть налево идут значения меньше данного, а направо больше, одинаковых значений быть не может.

Вывод дерева реализован через обход дерева в ширину, если указатель на корень дева равен NULL, то в таком случае выводится "-".

Также сложной операцией является удаление, потому что ест три разных случая, самым сложных является удаление, когда у узла есть два потомка. В когда у узла один или ноль потомков мы переносим указатель со значимого потомка к родителю класса. В случае, когда у удаляемого узла 2 потомка, то мы ставим на место удаляемого узла минимальный узел правого поддерева. Так же отдельно рассматриваеться случай когда удаляеться корневой элемент дерева.

### Работа программы

На русуноке ниже представлен результат работы программы с введением тестовых данных.

![Рисунок](img/1.jpg "Рисунок 1 - Результат работы программы")

Рисунок 1 - Результат работы программы

### Результат прохождения pipeline

 
 |pr5_easy_test|pr5_middle_test|
 |:---:|:---:|
 |☑|☑|
 
## Заключение

В ходе выполнения практической работы была написана система хранения динамических данных “бинарное дерево поиска”.

### Исходный код программы

```c
#include <stdio.h>
#include <stdlib.h>

typedef struct tree
{
    struct node *root;
    int count;
} tree;

typedef struct node
{
    int key;
    struct node *left;
    struct node *right;
    struct node *parent;
} node;


// Инициализация дерева
void init(tree* t) {
    t->root = NULL;
    t->count = 0;
}

//вставка
int insert(tree* t, int value) {  
    node *nodeNew = (node*) malloc(sizeof(node));
    node *node1 = t->root, *node2 = NULL;
    while (node1 != NULL) {
        node2 = node1;
        if (value < node1->key) {
            node1 = node1->left;
        } else if (value == node1->key) {
            return 1;
        } else {
            node1 = node1->right;
        }
    }
    nodeNew->key = value;
    nodeNew->left = NULL;
    nodeNew->right = NULL;
    nodeNew->parent = node2; 
    if (t->root == NULL) {
        t->root = nodeNew;
    }
    if (node2 != NULL) {
        if (value < node2->key) {;
            node2->left = nodeNew;
        } else  {
            node2->right = nodeNew;
        }
    }
    t->count++;
    return 0;
}

// Поиск элемента по значению. Вернуть NULL если элемент не найден
node* find(node* n, int value) {
    if (n == NULL || (n->key == value)) {
        return n;
    }
    if (value < n->key) {
        return find(n->left, value);
    } else {
        return find(n->right, value);
    }
}

typedef struct structItem{
    void *flag;
    struct structItem* next;
} item;

typedef struct structQueue{
    item* start;
    item* end;
} queue;

queue* createQueue(){
    queue* q = malloc(sizeof(queue));
    q->start = NULL;
    q->end = NULL;
    return q;
}

void addQueue(queue* q, void *flag){
    item *i = malloc(sizeof(item));
    i->next = NULL;
    i->flag = flag;

    if(q->end == NULL){
        q->start = i;
        q->end = i;
    }else{
        q->end->next = i;
        q->end = i;
    }
}

void* removeQueue(queue *q){
    if(q->start == NULL) return NULL;
    item *current = q->start;
    q->start = q->start->next;
    if(q->start == NULL) {
        q->end = NULL;
    }
    void* flag = current->flag;
    free(current);
    return flag;
}

void print(tree* t, node* root){
    queue* thisQ = NULL;
    queue* nextQ = createQueue();
    addQueue(nextQ, (void *) root);
    
    if (t->count == 0) {
        printf("-");
        printf("\n");
        return;
    }
    int isNotLastLevel;
    do{
        free(thisQ);
        thisQ = nextQ;
        nextQ = createQueue();
        void* flag;

        isNotLastLevel = 0;
        while(thisQ->start != NULL){
            flag = removeQueue(thisQ);
            if(flag != NULL){
                node* n = (node *)flag;
                printf("%d ", n->key);
                addQueue(nextQ, n->left);
                addQueue(nextQ, n->right);
                isNotLastLevel = isNotLastLevel || n->left || n->right;
            }else{
                printf("_ ");
                addQueue(nextQ, NULL);
                addQueue(nextQ, NULL);
            }
        }
        printf("\n");
    } while(isNotLastLevel);

    free(thisQ);
    while(nextQ->start != NULL) removeQueue(nextQ);
    free(nextQ);
}

void clean(tree* tr, node* t) {
    tr->count = 0;
    if (t->left != NULL) {
        clean(tr, t->left);
    }
    if (t->right != NULL) {
        clean(tr, t->right);
    }
    if (t->parent != NULL) {
        node* node3 = t->parent;
        if (node3->right == t) {
           node3->right = NULL;
        } else {
            node3->left = NULL;
        }
    } else {
        tr->root = NULL;
        return;
    }
    free(t);
}

node *min(node *root)
{
    node *l = root;
    while (l -> left != NULL)
        l = l -> left;
    return l;
}

void replaceParent(node* son, node* new) {
    if (son->parent != NULL) {
        node* parentNode = son->parent;
        if (parentNode->right == son) {
            parentNode->right = new;
        } else {
            parentNode->left = new;
        }
        if (new != NULL)
            new->parent = parentNode;
    } else {
        if (new != NULL) 
            new->parent = NULL;  
    }  
}

int remove_node(tree* t, int value) {
    
    node *node1 = t->root, *node2 = NULL, *node3 = NULL;
    node2 = find(node1, value); 
    
    if (node2 == NULL) {
        return 1;
    }

    if (node1->key == value) {
        if (node1->right == NULL && node1->left == NULL) {
            t->count--;
            free(t->root);
            return 0;
        } else if (node1->right != NULL && node1->left == NULL) {
            node1->right->parent = NULL;
            t->root = node1->right;
            t->count--;
            free(node1);
            return 0;
        } else if (node1->right == NULL && node1->left != NULL) {
            node1->left->parent = NULL;
            t->root = node1->left;
            t->count--;
            free(node1);
            return 0;
        }
    }
    if (node2->right == NULL && node2->left == NULL) {
        replaceParent(node2, NULL);
        free(node2);
        t->count--;
        return 0;
    } else if (node2->right != NULL && node2->left == NULL) {
        replaceParent(node2, node2->right);
        free(node2);
        t->count--;
        return 0;
    } else if (node2->right == NULL && node2->left != NULL) { 
        replaceParent(node2, node2->left);
        free(node2);
        t->count--;
        return 0;
    } else if ((node2->left != NULL) && (node2->right != NULL)) {
        node1 = min(node2->right);
        if (node1 != NULL) {
            int a = node1->key;
            remove_node(t, a);
            node2->key = a;
            return 0;
        }
    }
    return 1;
}

int remove_min(tree* t, node* n) {
    if (n == NULL) {
        return 0;
    }
    node *node1 = min(n);
    int a = node1->key;
    remove_node(t, a);
    t->count--;
    return a;
}

int rotate_right(tree* t, node* n) {
    if (n != NULL && n->left != NULL) {
        t->root = n->left;
        node *temp = n->left->right;
        n->left->right = n;
        n->parent = n->left;
        n->left->parent = NULL;
        n->left = temp;
        if (temp != NULL) 
            temp->parent = n;
        return 0;
    }
    return 1;
}

int rotate_left(tree* t, node* n) {
    if (n != NULL && n->right != NULL) {
        t->root = n->right;
        node *temp = n->right->left;
        n->right->left = n;
        n->parent = n->right;
        n->right->parent = NULL;
        n->right = temp;
        
        if (temp != NULL) 
            temp->parent = n;
        return 0;
    }
    return 1;
}

void print_tree(tree* t) {
    print(t, t->root);
}

void printFound(node* n) {
    if (n == NULL) {
        printf("-");
    }else {
        if (n->parent == NULL) {
            printf("_ "); 
        } else {
            printf("%d ", n->parent->key);
        }
        if (n->left == NULL) {
            printf("_ "); 
        } else {
            printf("%d ", n->left->key);
        }
        if (n->right == NULL) {
            printf("_"); 
        } else {
            printf("%d", n->right->key);
        }
    }
    printf("\n\n");
}


int main()
{
    tree t;
    init(&t);
    int a, a1, a2, a3;
    
    scanf("%d %d %d %d", &a, &a1, &a2, &a3);
    insert(&t, a);
    insert(&t, a1);
    insert(&t, a2);
    insert(&t, a3);
    print_tree(&t);
    printf("\n");
    
    scanf("%d %d %d", &a, &a1, &a2);
    insert(&t, a);
    insert(&t, a1);
    insert(&t, a2);
    print_tree(&t);
    printf("\n");
    
    scanf("%d", &a);
    node* n = find(t.root, a);
    printFound(n);
    
    scanf("%d", &a);
    n = find(t.root, a);
    printFound(n);
    
    scanf("%d", &a);
    remove_node(&t, a);
    print_tree(&t);
    printf("\n");

    a = 0;
    while (a == 0) {
       a = rotate_left(&t, t.root); 
    }
    print_tree(&t);
    printf("\n");
    a = 0;
    while (a == 0) {
       a = rotate_right(&t, t.root); 
    }
    print_tree(&t);
    printf("\n");

    printf("%d", t.count);
    printf("\n");
    printf("\n");
    clean(&t, t.root);
    print_tree(&t);
    printf("\n");
    return 0;
}
```