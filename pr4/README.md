# Практическая работа №4

## Задание

Паписать программу реализующею двусвязные списки, настроить Pipeline, составить отчет и залить на gitlab.

## Функции которые необходимо реализовать

Согласно задания, в программе необходимо реализовать следующие функции для работы со списками:

```c
// инициализация пустого списка
void init(list *l);

// удалить все элементы из списка
void clean(list *l);

// проверка на пустоту списка
bool is_empty(list *l);

// поиск элемента по значению. вернуть NULL если элемент не найден
node *find(list *l, int value);

// вставка значения в конец списка, вернуть 0 если успешно
int push_back(list *l, int value);

// вставка значения в начало списка, вернуть 0 если успешно
int push_front(list *l, int value);

// вставка значения после указанного узла, вернуть 0 если успешно
int insert_after(list *l, node *n, int value);

// вставка значения перед указанным узлом, вернуть 0 если успешно
int insert_before(list *l, node *n, int value);

// удалить первый элемент из списка с указанным значением, 
// вернуть 0 если успешно
int remove_first(list *l, int value);

// удалить последний элемент из списка с указанным значением, 
// вернуть 0 если успешно
int remove_last(list *l, int value);

// вывести все значения из списка в прямом порядке через пробел,
// после окончания вывода перейти на новую строку
void print(list *l);

// вывести все значения из списка в обратном порядке через пробел,
// после окончания вывода перейти на новую строку
void print_invers(list *l);

```

## Методика тестирования программы

Используя реализованные ранее функции, реализовать программу которая:

1. считывает количество элементов n, 0 < n <= 2147483647;
1. создает пустой список, считывает n элементов a, |a| <= 2147483647 и заносит их в список;
1. выводит содержимое списка, используя функцию print;
1. считывает k1, k2, k3 (|k| <= 2147483647) и выводит "1", если в списке существует элемент с таким значением и "0" если нет (выводить через пробел, например "1 0 1");
1. считывает m, |m| <= 2147483647 и вставляет его в конец списка;
1. выводит содержимое списка, используя функцию print_invers;
1. считывает t, |t| <= 2147483647 и вставляет его в начало списка;
1. выводит содержимое списка, используя функцию print;
1. считывает j и x (0 < j <= 2147483647, |x| <= 2147483647) и вставляет значение x после j-ого элемента списка;
1. выводит содержимое списка, используя функцию print_invers;
1. считывает u и y (0 < u <= 2147483647, |y| <= 2147483647) и вставляет значение y перед u-ым элементом списка;
1. выводит содержимое списка, используя функцию print;
1. считывает z, |z| <= 2147483647 и удаляет первый элемент (при его наличии), хранящий значение z из списка;
1. выводит содержимое списка, используя функцию print_invers;
1. считывает r, |r| <= 2147483647 и удаляет последний элемент (при его наличии), хранящий значение r из списка;
1. выводит содержимое списка, используя функцию print;
1. очищает список.

## Программа

Исходный код программы:

```c
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node {
    int value;          // значение, которое хранит узел 
    struct node *next;  // ссылка на следующий элемент списка
    struct node *prev;  // ссылка на предыдущий элемент списка
} node;

typedef struct list {
    struct node *head;  // начало списка
    struct node *tail;  // конец списка
} list;

// инициализация пустого списка
void init(list *l) {
    l->head = NULL;
    l->tail = NULL;
}

// удалить все элементы из списка
void clean(list *l) {
    node *next_t = l->head->next;
    while (next_t) {
        next_t = l->head->next;
        free(l->head);
        l->head = l->head->next;
    }
}

// проверка на пустоту списка
bool is_empty(list *l) {
    if (l->head == NULL) {
        return true;
    } else {
        return false;
    }
}

// поиск элемента по значению. вернуть NULL если элемент не найден
node *find(list *l, int value) {
    node* nodeN = l->head;
    while (nodeN) {
        if (nodeN->value == value) {
            return nodeN;
        }
        nodeN = nodeN->next;
    }
    return NULL;
}

// вставка значения в конец списка, вернуть 0 если успешно
int push_back(list *l, int value){
    node *nodeNew = (node*) malloc(sizeof(node));
    nodeNew->next = NULL;
    nodeNew->value = value;
    nodeNew->prev = l->tail;
    if(l->tail != NULL) {
        l->tail->next = nodeNew;
    } 
    if(l->tail == NULL) {
        l->head = nodeNew;
        l->tail = nodeNew;
        return 0;
    } else {
        l->tail = nodeNew;
        return 0;
    }
    return 1;
}

// вставка значения в начало списка, вернуть 0 если успешно
int push_front(list *l, int value) {
    node *nodeNew = (node*) malloc(sizeof(node));
    nodeNew->prev = NULL;
    nodeNew->value = value;
    nodeNew->next = l->head;
    if (l->head != NULL) {
        l->head->prev = nodeNew;
    } 
    if (l->head == NULL) {
        l->head = nodeNew;
        l->tail = nodeNew;
        return 0;
    } else {
        l->head = nodeNew;
        return 0;
    }
    return 1;
}

// вставка значения после указанного узла, вернуть 0 если успешно
int insert_after(list *l, node *n, int value) {
    if (n != NULL) {
        node* temp = n->next;
        node *nodeNew = (node*) malloc(sizeof(node));
        nodeNew->value = value;
        nodeNew->prev = n;
        n->next = nodeNew;
        if (n == l->tail) {
            l->tail = nodeNew;
        } else {
            temp->prev = nodeNew;
            nodeNew->next = temp;
        }
        return 0;
    } else {
        return 1;
    }
}

// вставка значения перед указанным узлом, вернуть 0 если успешно
int insert_before(list *l, node *n, int value) {
    if (n != NULL) {
        node *nodeNew = (node*) malloc(sizeof(node));
        node* temp = n->prev;
        nodeNew->value = value;
        nodeNew->next = n;
        n->prev = nodeNew;
        if (n == l->head) {
            l->head = nodeNew;
        } else {
            temp->next = nodeNew;
            nodeNew->prev = temp;
        }
        return 0;
    } else {
        return 1;
    }
}

// удалить первый элемент из списка с указанным значением, 
// вернуть 0 если успешно
int remove_first(list *l, int value) {
    node* n = l->head;
    while(n->value != value && n != NULL) {
        if (n->next == NULL) {
            return 1;
        }
        n = n->next;
    } 
    if (n == NULL) {
        return 1;
    } 
    if (n == l->tail) {
        l->tail = n->prev;
        l->tail->next = NULL;
    } else if(n == l->head) {
        l->head = n->next;
        l->head->prev = NULL;
    } else {
        node* nN = n->next;
        node* nP = n->prev;
        nN->prev = nP;
        nP->next = nN;
    }
    free(n);
    return 0;
}

// удалить последний элемент из списка с указанным значением, 
// вернуть 0 если успешно
int remove_last(list *l, int value) {
    node* n = l->tail; 
    while (n->value != value && n != NULL) {
        if (n->prev == NULL) {
            return 1;
        }
        n = n->prev;
    }    
    if (n == NULL) {
        return 1;
    }  
    if (n == l->tail) {
        l->tail = n->prev;
        l->tail->next = NULL;
    } else if(n == l->head) {
        l->head = n->next;
        l->head->prev = NULL;
    } else {
        node* nN = n->next;
        node* nP = n->prev;
        nN->prev = nP;
        nP->next = nN;
    }
    free(n);
    return 0;
}

// вывести все значения из списка в прямом порядке через пробел,
// после окончания вывода перейти на новую строку
void print(list *l) {
    node *temp = l->head;
    while (l->head) {
        printf("%d ", l->head->value);
        l->head = l->head->next;
    }
    l->head = temp;
    printf(" \n");
}

// вывести все значения из списка в обратном порядке через пробел,
// после окончания вывода перейти на новую строку
void print_invers(list *l) {
    node *temp = l->tail;
    while (l->tail) {
        printf("%d ", l->tail->value);
        l->tail = l->tail->prev;
    }
    l->tail = temp;
    printf(" \n");
}

node* find_node_by_index(list *l, int index) {
    node* temp = l->head;
    for (int i = 0; i < index - 1; i++) {
        if (temp->next == NULL) {
            return l->tail;
        }
        temp = temp->next;
    }
    return temp;
}

int main() {
    int n, s, s1, s2;
    list l;
    init(&l);
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d", &s);
        push_back(&l, s);
    }
       
    print(&l);
    scanf("%d %d  %d", &s, &s1, &s2);
    printf("%d %d %d\n", find(&l, s) != NULL, find(&l, s1) != NULL, find(&l, s2) != NULL);

    scanf("%d", &s);
    push_back(&l, s);
    print_invers(&l);
    
    scanf(" %d", &s);
    push_front(&l, s);
    print(&l);
    
    scanf(" %d %d", &s, &s1);
    node* nod = find_node_by_index(&l, s);
    insert_after(&l, nod, s1);
    print_invers(&l);
    
    scanf(" %d %d", &s, &s1);
    nod = find_node_by_index(&l, s);
    insert_before(&l, nod, s1);
    print(&l);
    
    scanf(" %d", &s);
    remove_first(&l, s);
    print_invers(&l);
    
    scanf(" %d", &s);
    remove_last(&l, s);
    print(&l);
    
    clean(&l);
    return 0;
};


```


## Результаты

В ходе работы была написана программа реалезующя двусвязные списки, настроен Pipeline, результаты были загружены на GitLab.
