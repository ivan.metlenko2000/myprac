#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node {
    int value;          // значение, которое хранит узел
    struct node *next;  // ссылка на следующий элемент списка
    struct node *prev;  // ссылка на предидущий элемент списка
} node;

typedef struct list {
    struct node *head;  // начало списка
    struct node *tail; // конец списка
} list;

// инициализация пустого списка
void init(list *l);

// удалить все элементы из списка
void clean(list *l);

// проверка на пустоту списка
bool is_empty(list *l);

// поиск элемента по значению. вернуть NULL если элемент не найден
node *find(list *l, int value);

// вставка значения в конец списка, вернуть 0 если успешно
int push_back(list *l, int value);

// вставка значения в начало списка, вернуть 0 если успешно
int push_front(list *l, int value);

// вставка значения после указанного узла, вернуть 0 если успешно
int insert_after(list *l, node *n, int value);

// вставка значения перед указанным узлом, вернуть 0 если успешно
int insert_before(list *l, node *n, int value);

// удалить первый элемент из списка с указанным значением,
// вернуть 0 если успешно
int remove_first(list *l, int value);

// удалить последний элемент из списка с указанным значением,
// вернуть 0 если успешно
int remove_last(list *l, int value);

// вывести все значения из списка в прямом порядке через пробел,
// после окончания вывода перейти на новую строку
void print(list *l);

// вывести все значения из списка в обратном порядке через пробел,
// после окончания вывода перейти на новую строку
void print_invers(list *l);

//Поиск ноды по номеру
node *find_by_id(list *l, int id);


int main(void) {
    list ulist;
    init(&ulist);
    int n, i, *flowinput, input;
    (void) scanf("%d", &n);
    flowinput = malloc(sizeof(int)*n);
    i = 0;
    while(scanf("%d", &flowinput[i++]) == 1) {
        (void) push_back(&ulist, flowinput[i-1]);
        if(i == n) {
            free(flowinput);
            break;
        }
    }
    print(&ulist);

    flowinput = malloc(sizeof(int)*3);
    i = 0;
    while(scanf("%d", &flowinput[i++]) == 1) {
        if(find(&ulist, flowinput[i-1]) == NULL) {
            printf("%d ", 0);
        } else {
            printf("%d ", 1);
        }
        if(i == 3) {
            free(flowinput);
            printf("%s", "\n");
            break;
        }
    }

    (void) scanf("%d", &input);
    (void) push_back(&ulist, input);
    print_invers(&ulist);

    (void) scanf("%d", &input);
    (void) push_front(&ulist, input);
    print(&ulist);

    flowinput = malloc(sizeof(int)*2);
    i = 0;
    while(scanf("%d", &flowinput[i++]) == 1) {
        if(i == 2) {
            (void) insert_after(&ulist, find_by_id(&ulist, flowinput[0]), flowinput[1]);
            free(flowinput);
            break;
        }
    }
    print_invers(&ulist);

    flowinput = malloc(sizeof(int)*2);
    i = 0;
    while(scanf("%d", &flowinput[i++]) == 1) {
        if(i == 2) {
            (void) insert_before(&ulist, find_by_id(&ulist, flowinput[0]), flowinput[1]);
            free(flowinput);
            break;
        }
    }
    print(&ulist);

    (void) scanf("%d", &input);
    (void) remove_first(&ulist, input);
    print_invers(&ulist);

    (void) scanf("%d", &input);
    (void) remove_last(&ulist, input);
    print(&ulist);

    return 0;
}

void init(list *l) {
    l->head = NULL;
    l->tail = NULL;
}

void clean(list *l) {
    node *currentnode = l->head;
    node *nextnode = currentnode->next;
    while(nextnode != NULL) {
        free(currentnode);
        currentnode = nextnode;
        nextnode = currentnode->next;
    }
    l->head = NULL;
    l->tail = NULL;
}

bool is_empty(list *l) {
    if(l->head == NULL && l->tail == NULL) {
        return true;
    }
    return false;
}

node *find(list *l, int value) {
    node *currentnode = l->head;
    while(currentnode != NULL) {
        if(currentnode->value == value) {
            return currentnode;
        }
        currentnode = currentnode->next;
    }
    return NULL;
}

int push_back(list *l, int value) {
    node *insert;
    insert = malloc(sizeof(node));
    insert->value = value;
    insert->next = NULL;
    insert->prev = l->tail; // Если в спикске один элемент то prev и next = NULL
    if(l->head == NULL) {
        l->head = insert;
        l->tail = insert;
    } else {
        l->tail->next = insert;
        l->tail = insert;
    }
    return 0;
}

int push_front(list *l, int value) {
    node *insert;
    insert = malloc(sizeof(node));
    insert->value = value;
    insert->next = l->head;
    insert->prev = NULL;
    if(l->head == NULL) {
        l->head = insert;
        l->tail = insert;
    } else {
        l->head->prev = insert;
        l->head = insert;
    }
    return 0;
}

int insert_after(list *l, node *n, int value) {
    node *insert;
    insert = malloc(sizeof(node));
    insert->value = value;
    insert->next = n->next;
    insert->prev = n;
    n->next->prev = insert;
    n->next = insert;
    if(insert->next == NULL) {
        l->head = insert;
    }
    return 0;
}

int insert_before(list *l, node *n, int value) {
    node *insert;
    insert = malloc(sizeof(node));
    insert->value = value;
    insert->next = n;
    insert->prev = n->prev;
    n->prev->next = insert;
    n->prev = insert;
    if(insert->prev == NULL) {
        l->head = insert;
    }
    return 0;
}

int remove_first(list *l, int value) {
    node *currentnode = l->head;
    while(currentnode->value != value) {
        currentnode = currentnode->next;
        if(currentnode == NULL) {
            break;
        }
    }
    if(currentnode != NULL) {
        //Если попали в начало
        if(currentnode->prev == NULL) {
            l->head = currentnode->next;
        } else {
            currentnode->prev->next = currentnode->next;
        }
        //Если попали в конец
        if(currentnode->next == NULL) {
            l->tail = currentnode->prev;
        } else {
            currentnode->next->prev = currentnode->prev;
        }
        free(currentnode);
    } else {
        return 1;
    }
    return 0;
}

int remove_last(list *l, int value) {
    node *currentnode = l->tail;
    while(currentnode->value != value) {
        currentnode = currentnode->prev;
        if(currentnode == NULL) {
            break;
        }
    }
    if(currentnode != NULL) {
        //Если попали в начало
        if(currentnode->prev == NULL) {
            l->head = currentnode->next;
        } else {
            currentnode->prev->next = currentnode->next;
        }
        //Если попали в конец
        if(currentnode->next == NULL) {
            l->tail = currentnode->prev;
        } else {
            currentnode->next->prev = currentnode->prev;
        }
        free(currentnode);
    } else {
        return 1;
    }
    return 0;
}

void print(list *l) {
    node *currentnode = l->head;
    while(currentnode != NULL) {
        printf("%d ", currentnode->value);
        currentnode = currentnode->next;
    }
    printf("%s", "\n");
}

void print_invers(list *l) {
    node *currentnode = l->tail;
    while(currentnode != NULL) {
        printf("%d ", currentnode->value);
        currentnode = currentnode->prev;
    }
    printf("%s", "\n");
}

node *find_by_id(list *l, int id) {
    node *next = l->head;
    int i;
    for(i = 1; i < id; i++) {
        next = next->next;
    }
    return next;
}
