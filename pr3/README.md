# Практическая работа №3

## Задание

Паписать программу реализующею односвязные списки, настроить Pipeline, составить отчет и залить на gitlab.

## Функции которые необходимо реализовать

Согласно задания, в программе необходимо реализовать следующие функции для работы со списками:

```c
// инициализация пустого списка
void init(list *l);

// удалить все элементы из списка
void clean(list *l);

// проверка на пустоту списка
bool is_empty(list *l);

// поиск элемента по значению. вернуть NULL если эжемент не найден
node *find(list *l, int value);

// вставка значения в конец списка, вернуть 0 если успешно
int push_back(list *l, int value);

// вставка значения в начало списка, вернуть 0 если успешно
int push_front(list *l, int value);

// вставка значения после указанного узла, вернуть 0 если успешно
int insert_after(node *n, int value);

// удалить первый элемент из списка с указанным значением, 
// вернуть 0 если успешно
int remove_node(list *l, int value);

// вывести все значения из списка в прямом порядке через пробел,
// после окончания вывода перейти на новую строку
void print(list *l);

```

## Методика тестирования программы

Используя реализованные ранее функции, реализовать программу которая:

1. считывает количество элементов n, 0 < n <= 2147483647;
1. создает пустой список, считывает n элементов a, |a| <= 2147483647 и заносит их в список;
1. выводит содержимое списка, используя функцию print;
1. считывает k1, k2, k3 (|k| <= 2147483647) и выводит "1", если в списке существует элемент с таким значением и "0", если нет (выводить через пробел, например "1 0 1");
1. считывает m, |m| <= 2147483647 и вставляет его в конец списка;
1. выводит содержимое списка, используя функцию print;
1. считывает t, |t| <= 2147483647 и вставляет его в начало списка;
1. выводит содержимое списка, используя функцию print;
1. считывает j и x (0 < j <= 2147483647, |x| <= 2147483647) и вставляет значение x после j-ого элемента списка;
1. выводит содержимое списка, используя функцию print;
1. считывает z, |z| <= 2147483647 и удаляет первый элемент (при его наличии), хранящий значение z из списка;
1. выводит содержимое списка, используя функцию print;
1. очищает список.

## Программа

Исходный код программы:

```c
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct node {
    int value;
    struct node *next;
} node;

typedef struct list {
    struct node *head;
} list;

// инициализация пустого списка
void init(list *l);

// удалить все элементы из списка
void clean(list *l);

// проверка на пустоту списка
bool is_empty(list *l);

// поиск элемента по значению. вернуть NULL если элемент не найден
node *find(list *l, int value);

// вставка значения в конец списка, вернуть 0 если успешно
int push_back(list *l, int value);

// вставка значения в начало списка, вернуть 0 если успешно
int push_front(list *l, int value);

// вставка значения после указанного узла, вернуть 0 если успешно
int insert_after(node *n, int value);

// удалить первый элемент из списка с указанным значением,
// вернуть 0 если успешно
int remove_node(list *l, int value);

// вывести все значения из списка в прямом порядке через пробел,
// после окончания вывода перейти на новую строку
void print(list *l);

//Поиск ноды по номеру
node *find_by_id(list *l, int id);


int main() {
    list ulist;
    init(&ulist);
    int n, i, *flowinput, input;
    (void) scanf("%d", &n);
    flowinput = malloc(sizeof(int)*n);
    i = 0;
    while(scanf("%d", &flowinput[i++]) == 1) {
        (void) push_back(&ulist, flowinput[i-1]);
        if(i == n) {
            free(flowinput);
            break;
        }
    }
    print(&ulist);

    flowinput = malloc(sizeof(int)*3);
    i = 0;
    while(scanf("%d", &flowinput[i++]) == 1) {
        if(find(&ulist, flowinput[i-1]) == NULL) {
            printf("%d ", 0);
        } else {
            printf("%d ", 1);
        }
        if(i == 3) {
            free(flowinput);
            printf("%s", "\n");
            break;
        }
    }

    (void) scanf("%d", &input);
    (void) push_back(&ulist, input);
    print(&ulist);

    (void) scanf("%d", &input);
    (void) push_front(&ulist, input);
    print(&ulist);

    flowinput = malloc(sizeof(int)*2);
    i = 0;
    while(scanf("%d", &flowinput[i++]) == 1) {
        if(i == 2) {
            (void) insert_after(find_by_id(&ulist, flowinput[0]), flowinput[1]);
            free(flowinput);
            break;
        }
    }
    print(&ulist);

    (void) scanf("%d", &input);
    (void) remove_node(&ulist, input);
    print(&ulist);

    clean(&ulist);

    return 0;
}

void init(list *l) {
    l->head = NULL;
}

void clean(list *l) {
    node *currentnode = l->head;
    node *nextnode = currentnode->next;
    while(nextnode != NULL) {
        free(currentnode);
        currentnode = nextnode;
        nextnode = currentnode->next;
    }
    l->head = NULL;
}

bool is_empty(list *l) {
    if(l->head == NULL) {
        return true;
    }
    return false;
}

node *find(list *l, int value) {
    node *currentnode = l->head;
    while(currentnode != NULL) {
        if(currentnode->value == value) {
            return currentnode;
        }
        currentnode = currentnode->next;
    }
    return NULL;
}

int push_back(list *l, int value) {
    node *insert;
    insert = malloc(sizeof(node));
    insert->value = value;
    insert->next = NULL;
    if(l->head == NULL) {
        l->head = insert;
    } else {
        node *next = l->head;
        while(next->next != NULL) {
            next = next->next;
        }
        next->next = insert;
    }
    return 0;
}

int push_front(list *l, int value) {
    node *insert;
    insert = malloc(sizeof(node));
    insert->value = value;
    if(l->head == NULL) {
        insert->next = NULL;
        l->head = insert;
    } else {
        node *oldhead = l->head;
        insert->next = oldhead;
        l->head = insert;
    }
    return 0;
}

int insert_after(node *n, int value) {
    node *insert;
    insert = malloc(sizeof(node));
    insert->value = value;
    node *oldnext = n->next;
    insert->next = oldnext;
    n->next = insert;
    return 0;
}

int remove_node(list *l, int value) {
    node *currentnode = l->head;
    node *prenode = NULL;
    node *newnext;
    while(currentnode != NULL) {
        if(currentnode->value == value) {
            break;
        }
        prenode = currentnode;
        currentnode = currentnode->next;
    }
    if(currentnode == NULL) {
        return 1;
    }
    newnext = currentnode->next;
    if(prenode == NULL) {
        free(currentnode);
        l->head = newnext;
    } else {
        free(currentnode);
        prenode->next = newnext;
    }
    return 0;
}

void print(list *l) {
    node *currentnode = l->head;
    while(currentnode != NULL) {
        printf("%d ", currentnode->value);
        currentnode = currentnode->next;
    }
    printf("%s", "\n");
}

node *find_by_id(list *l, int id) {
    node *next = l->head;
    int i;
    for(i = 1; i < id; i++) {
        next = next->next;
    }
    return next;
}

```


## Результаты

В ходе работы была написана программа реалезующя односвязные списки, настроен Pipeline, результаты были загружены на GitLab.
